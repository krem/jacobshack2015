package stache.pl.friendshake;

import android.app.Activity;
import android.content.Context;
import android.hardware.camera2.params.Face;
import android.net.Uri;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by kremb on 2015-10-17.
 */
public class FacebookUtils {
    static private AccessTokenTracker accessTokenTracker;
    static private CallbackManager mFbCallbackManager;

    static public CallbackManager initFacebook(LoginButton loginButton, final Runnable setText, Activity activity) {
        if (mFbCallbackManager == null) mFbCallbackManager = CallbackManager.Factory.create();
        if (accessTokenTracker == null) {
            if (AccessToken.getCurrentAccessToken() == null)
                activity.runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                AccessToken.refreshCurrentAccessTokenAsync();
                            }
                        });
        }
        loginButton.registerCallback(mFbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                setText.run();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        return mFbCallbackManager;
    }

    static public String getInfoAboutMe() {
        if (AccessToken.getCurrentAccessToken() == null) return null;
        Profile profile = Profile.getCurrentProfile();
        Uri personalLink = profile.getLinkUri();
        String name = profile.getName();
        Uri photoLink = profile.getProfilePictureUri(512, 512);
        if (personalLink == null || name == null || photoLink == null)
            return null;
        else {
            StringBuilder str = new StringBuilder();
            str.append(personalLink.toString());
            str.append('\b');
            str.append(name.toString());
            str.append('\b');
            str.append(photoLink.toString());
            return str.toString();
        }
    }

    static public FacebookPersonalDetails getInfoFromString(String input) {
        String[] res = input.split("\b");
        if(res.length != 3) return null;
        FacebookPersonalDetails result = new FacebookPersonalDetails();
        result.personalUri = Uri.parse(res[0]);
        result.name = res[1];
        result.photoUri = Uri.parse(res[2]);
        return result;
    }
}
