package stache.pl.friendshake;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private NfcAdapter mNfcAdapter;
    private TextView mTextView;
    private LoginButton mLoginButton;
    private ImageView mProfilePicture;
    public static final String MIME_TYPE_FRIENDSHAKE = "application/stache.pl.friendshake";
    private CallbackManager mFbCallbackManager;
    public static final String MIME_TEXT_PLAIN = "application/stache.pl.friendshake";
    public static final String TAG = "NfcDemo";
    private int i = 0;
    private byte[] data;
    private String readData = "";
    private ProgressDialog pDialog;
    private String currentInfo = "No information provided yet.";
    public static final int progress_bar_type = 0;
    private NfcNdefTagReader.NFCListener listener;
    private FileDownloader.ProgressUpdater updater;
    private static Map<String, Drawable> cache = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        final Button saveButton = (Button) findViewById(R.id.prepareForSendingButton);
        mProfilePicture = (ImageView) findViewById(R.id.profilePicture);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String link = FacebookUtils.getInfoAboutMe();
                data = (link != null) ? link.getBytes() : data;
            }
        });

        mTextView = (TextView) findViewById(R.id.messageBox);
        mLoginButton = (LoginButton) findViewById(R.id.login_button);
        mFbCallbackManager = FacebookUtils.initFacebook(
                mLoginButton,
                new Runnable() {
                    @Override
                    public void run() {
                        data = FacebookUtils.getInfoAboutMe().getBytes();
                    }
                },
                this);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        mNfcAdapter.setNdefPushMessageCallback(new NfcAdapter.CreateNdefMessageCallback() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public NdefMessage createNdefMessage(NfcEvent event) {
                i++;
                updateText();
                return new NdefMessage(NdefRecord.createMime(MIME_TYPE_FRIENDSHAKE, data));
            }
        }, this);

        if (mNfcAdapter == null) {
            mTextView.setText("NFC is not available on this device...");
        } else if (!mNfcAdapter.isEnabled()) {
            mTextView.setText("NFC is disabled.");
        } else {
            mTextView.setText(R.string.explanation);
        }

        String link = FacebookUtils.getInfoAboutMe();
        data = (link != null) ? link.getBytes() : "DUPA08".getBytes();

        updater = new FileDownloader.ProgressUpdater() {
            @Override
            public void onProcessStart() {
                showDialog(progress_bar_type);
            }

            @Override
            public void onProgressUpdate(long progress) {
                pDialog.setProgress((int) progress);
            }

            @Override
            public void onDownloadFinished(String url, ByteArrayOutputStream result) {
                dismissDialog(progress_bar_type);
                if (result != null) {
                    updateText("DOWNLOAD COMPLETE! AND YZ NOT NULL AND HAZ " + result.toByteArray().length + " bytez.");
                    storeImage(url, result);
                    refreshImage(url);
                } else {
                    updateText("ERROREN!");
                }
            }
        };

        listener = new NfcNdefTagReader.NFCListener() {
            @Override
            public void onMessageReceived(String message) {
                FacebookPersonalDetails details = FacebookUtils.getInfoFromString(message);
                if(details != null) {
                    updateText(details.name);
                    setImage(details.photoUri.toString());
                } else {
                    updateText(message);
                }
            }
        };

        setImage("http://lh6.ggpht.com/s_VyyVsph5meqhCeEGjTCM1cbzTfWr6rUpQmINYrktB18aHES2QQ7LxD6QrvPA-7i_glG54dQRCvUBFYT38SVDAO=s800");
        handleIntent(getIntent());
    }

    private void refreshImage(String url) {
        if (cache.containsKey(url)) {
            mProfilePicture.setImageDrawable(cache.get(url));
        }
    }

    private void storeImage(String url, ByteArrayOutputStream data) {
        Drawable drawable = Drawable.createFromStream(new ByteArrayInputStream(data.toByteArray()), "");
        cache.put(url, drawable);
    }

    private void setImage(String url) {
        refreshImage(url);
        if (!cache.containsKey(url)) {
            new FileDownloader(updater).execute(url);
        }
    }

    private void updateText(String newData) {
        readData = newData;
        updateText();
    }

    private void updateText() {
        if (mTextView == null) mTextView = (TextView) findViewById(R.id.messageBox);
        currentInfo = "Send messages: " + i + "\nLast read message: " + readData;
        mTextView.post(new Runnable() {
            public void run() {
                mTextView.setText(currentInfo);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onPause() {
        stopForegroundDispatch(this, mNfcAdapter);
        super.onPause();

        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            String type = intent.getType();
            if (MIME_TYPE_FRIENDSHAKE.equals(type)) {
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NfcNdefTagReader(listener).execute(tag);
            } else {
                Log.d(NfcNdefTagReader.TAG, "Wrong mime type: " + type);
            }
        }
    }

    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TYPE_FRIENDSHAKE);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
