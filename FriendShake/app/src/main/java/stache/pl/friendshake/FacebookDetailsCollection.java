package stache.pl.friendshake;

import java.util.ArrayList;
import java.util.List;

public class FacebookDetailsCollection {
    private static FacebookDetailsCollection instance;
    private List<FacebookPersonalDetails> listOfDetails;

    private FacebookDetailsCollection() {
        listOfDetails = new ArrayList<>();
    }

    public static FacebookDetailsCollection getInstance() {
        if(instance == null) instance = new FacebookDetailsCollection();
        return instance;
    }

    public List<FacebookPersonalDetails> getListOfDetails() {
        return listOfDetails;
    }
}
