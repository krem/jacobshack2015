package stache.pl.friendshake;

import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.util.Log;

import java.io.UnsupportedEncodingException;

public class NfcNdefTagReader extends AsyncTask<Tag, Void, String> {
    public static final String TAG = "NfcDemo";

    public interface NFCListener {
        void onMessageReceived(String message);
    }

    private NFCListener listener;

    public NfcNdefTagReader(NFCListener _listener) {
        listener = _listener;
    }

    @Override
    protected String doInBackground(Tag... params) {
        Tag tag = params[0];

        Ndef ndef = Ndef.get(tag);
        if (ndef == null) {
            return null;
        }

        NdefRecord[] records = ndef.getCachedNdefMessage().getRecords();
        for (NdefRecord ndefRecord : records) {
            try {
                return readText(ndefRecord);
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "Unsupported Encoding", e);
            }
        }

        return null;
    }

    private String readText(NdefRecord record) throws UnsupportedEncodingException {
        byte[] payload = record.getPayload();
        return new String(payload, 0, payload.length, "UTF-8");
    }

    @Override
    protected void onPostExecute(String result) {
        listener.onMessageReceived(result);
    }
}
