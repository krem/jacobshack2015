package stache.pl.friendshake;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class FileDownloader extends AsyncTask<String, Integer, ByteArrayOutputStream> {
    private ProgressUpdater updater;
    private String url;

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public interface ProgressUpdater {
        void onProcessStart();
        void onProgressUpdate(long progress);
        void onDownloadFinished(String url, ByteArrayOutputStream result);
    }

    public FileDownloader(ProgressUpdater _updater) {
        updater = _updater;
        disableSSLCertificateChecking();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        updater.onProcessStart();
    }

    @Override
    protected ByteArrayOutputStream doInBackground(String... f_url) {
        ByteArrayOutputStream output = null;
        url = f_url[0];
        int count;
        try {
            output = new ByteArrayOutputStream();
            URL url = new URL(f_url[0]);
            long length = 0;
            URLConnection connection = url.openConnection();
            connection.connect();
            length = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                updater.onProgressUpdate((total*100)/length);
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            Log.e("Network file error: ", e.getMessage());
            return null;
        }
        return output;
    }

    @Override
    protected void onProgressUpdate(Integer... progressValues) {
        super.onProgressUpdate(progressValues);
        updater.onProgressUpdate(progressValues[0]);
    }

    @Override
    protected void onPostExecute(ByteArrayOutputStream data) {
        super.onPostExecute(data);
        updater.onDownloadFinished(url, data);
    }
}
