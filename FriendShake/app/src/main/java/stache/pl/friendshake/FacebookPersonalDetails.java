package stache.pl.friendshake;

import android.net.Uri;

/**
 * Created by kremb on 2015-10-18.
 */
public class FacebookPersonalDetails {
    public Uri personalUri;
    public String name;
    public Uri photoUri;
}
